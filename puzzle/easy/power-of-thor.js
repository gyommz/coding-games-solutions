steps = readline().split(' ')

x = +steps[0] - steps[2]
y = +steps[1] - steps[3]

sn = y>0?'S':(y=-y,'N')
ew = x>0?'E':(x=-x,'W')

while(1){
  move = (y?(y--, sn):'') 
       + (x?(x--, ew):'')
       
  console.log( move )
}
import math._
import scala.util._
import scala.io.StdIn._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {

    var checked = false;
    var pieces:Array[Piece] = new Array[Piece](5);

    class Piece(var row: Int, var col: Int, var symbol: String) {
        this.row = row;
        this.col = col;
        this.symbol = symbol;
    }

    def setAttackersPosition (row : String, col: Int) = {
        

        if (row.contains('B')) {
            val bishop = new Piece(col, row.indexOf('B') / 2, "bishop")
            pieces(1) = bishop
        }
        if (row.contains('N')) {
            val knigth = new Piece(col, row.indexOf('N') / 2, "knigth")
            pieces(1) = knigth
        }
        if (row.contains('R')) {
            val rook = new Piece(col, row.indexOf('R') / 2, "rook")
            pieces(1) = rook
        }
        if (row.contains('Q')) {
            val queen = new Piece(col, row.indexOf('Q') / 2, "queen")
            pieces(1) = queen
        }
    }

    def check = (boolean: Boolean) => {

        if (boolean == true) {
            println("Check");
        }  else {
            println("No Check");
        }
    }

    def compassAttack = (attacker: Piece, king: Piece) => {
    
        if (attacker.row == king.row || attacker.col == king.col) {
            checked = true;
        } 
    }

    def diagonalAttack = (attacker: Piece, king: Piece) => {
        val resX = Math.abs(attacker.col - king.col);
        val resY = Math.abs(attacker.row - king.row);
    
        if (resX == resY) {
            checked = true;
        } 
    }

    def knightAttack = (attacker: Piece, king: Piece) => {
        if (attacker.col - 1 == king.col && attacker.row - 2 == king.row) checked = true;
        if (attacker.col - 1 == king.col && attacker.row + 2 == king.row) checked = true;
        if (attacker.col - 2 == king.col && attacker.row - 1 == king.row) checked = true;
        if (attacker.col - 2 == king.col && attacker.row + 1 == king.row) checked = true;
        if (attacker.col + 1 == king.col && attacker.row - 2 == king.row) checked = true;
        if (attacker.col + 1 == king.col && attacker.row + 2 == king.row) checked = true;
        if (attacker.col - 2 == king.col && attacker.row - 1 == king.row) checked = true;
        if (attacker.col + 2 == king.col && attacker.row + 1 == king.row) checked = true;
    }

    for(i <- 0 until 8) {
        val chessRow = readLine

        if (chessRow.contains('K')) {
            val king = new Piece(i, chessRow.indexOf('K') / 2, "king")
            pieces(0) = king
        }

        setAttackersPosition(chessRow, i)
    }
    
    isCheck(pieces(1), pieces(0))

    def isCheck(attacker: Piece, king: Piece) {
        if (king != null) {
            attacker.symbol match {
                case "rook" =>  {
                    compassAttack(attacker, king);
                    check(checked);
                }
                case "bishop" => {
                    diagonalAttack(attacker, king);
                    check(checked);
                }
                case "knigth" => {
                    knightAttack(attacker, king);
                    check(checked);
                }
                case "queen" => {
                    compassAttack(attacker, king);
                    diagonalAttack(attacker, king);
                    check(checked);
                }
            }
        }
    }
   
    
    // Write an answer using println
    // To debug: Console.err.println("Debug messages...")
}
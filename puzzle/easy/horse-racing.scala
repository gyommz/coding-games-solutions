import math._
import scala.util._
import scala.io.StdIn._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {

    val n = readLine.toInt
    val horses = new Array[Int](n)
    for(i <- 0 until n) {
        val pi = readLine.toInt
        horses(i) = pi
        Console.err.println(pi)
    }
    def horseDiff(horses: Array[Int]): Unit = {
        val sorted = horses.sorted
        var smallestDiff = sorted(1) - sorted(0);
        for(i <- 2 until sorted.length) {
            var newDiff = Math.abs(sorted(i) - sorted(i - 1));
            if( newDiff < smallestDiff){
                smallestDiff = newDiff
            }
        }
        println(smallestDiff)
    }

    horseDiff(horses)
    
    // Write an answer using println
    // To debug: Console.err.println("Debug messages...")
}

import math._
import scala.util._
import scala.io.StdIn._

object Solution extends App {
  val n = readLine.toInt
  val horses = (1 to n)
    .map(_ => readLine.split(" ").map(_.toInt))
    .map(arr => (arr(0), arr(1)))

  def distance(horse1: (Int, Int), horse2: (Int, Int)): Int =
    math.abs(horse2._1 - horse1._1) + math.abs(horse2._2 - horse1._2)

  val minDistance =
    horses.combinations(2).map(pair => distance(pair(0), pair(1))).min

  println(minDistance)
}

import math._
import scala.util._
import scala.io.StdIn._
import scala.collection.mutable.ArrayBuffer

case class Exit(val floor: Int, val pos: Int)

case class Elevator(val floor: Int, val pos: Int)

class Clone(val direction: String, val floor: Int, val pos: Int) {
    var leading = true

    def setup(): Unit = {
        this.direction match {
            case "LEFT"  => if(this.pos == 1) println("BLOCK") else println("WAIT")
            case "RIGHT" => if(this.pos == Player.width - 1) println("BLOCK") else println("WAIT")
        }
    }
   
    def exitOnFloor(exit: Exit): Boolean = {
        this.floor == exit.floor
    }

    def elevatorInDirection(elevator: Elevator): Boolean = {
        this.direction match {
            case "LEFT"  => elevator.pos - 1 < this.pos 
            case "RIGHT" => elevator.pos + 1 > this.pos
        }
    }

    def exitInDirection(exit: Exit): Boolean = {
        if(exitOnFloor(exit))
        this.direction match {
            case "LEFT"  => exit.pos - 1 < this.pos
            case "RIGHT" => exit.pos + 1 > this.pos
        }
        else false
    }

    def play(elevator: Option[Elevator], exit: Exit): Unit = {
        if(exitOnFloor(exit) && exitInDirection(exit)) println("WAIT") 
        if(exitOnFloor(exit) && !exitInDirection(exit)) 
            println("BLOCK")
            this.leading = false
        elevator match {
            case Some(ele) => 
                if(elevatorInDirection(Elevator(ele.floor, ele.pos))) {
                    println("WAIT") 
                }
                if(!elevatorInDirection(Elevator(ele.floor, ele.pos))) {
                    println("BLOCK")
                    this.leading = false
                } 
                   
            case (_) => 
        }
    }
}

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Player extends App {
    // nbFloors: number of floors
    // width: width of the area
    // nbRounds: maximum number of rounds
    // exitFloor: floor on which the exit is found
    // exitPos: position of the exit on its floor
    // nbTotalClones: number of generated clones
    // nbAdditionalElevators: ignore (always zero)
    // nbElevators: number of elevators
    val Array(nbFloors, width, nbRounds, exitFloor, exitPos, nbTotalClones, nbAdditionalElevators, nbElevators) = (readLine split " ").filter(_ != "").map (_.toInt)
    val exit = new Exit(exitFloor, exitPos)
    val elevators = new Array[Elevator](nbElevators)
    val clones = ArrayBuffer[Clone]()
    for(i <- 0 until nbElevators) {
        // elevatorFloor: floor on which this elevator is found
        // elevatorPos: position of the elevator on its floor
        val Array(elevatorFloor, elevatorPos) = (readLine split " ").filter(_ != "").map (_.toInt)
        elevators(i) = Elevator(elevatorFloor, elevatorPos)
    }

    // game loop
    while(true) {
        
        // cloneFloor: floor of the leading clone
        // clonePos: position of the leading clone on its floor
        // direction: direction of the leading clone: LEFT or RIGHT
        val Array(_cloneFloor, _clonePos, direction) = readLine split " "
        val cloneFloor = _cloneFloor.toInt
        val clonePos = _clonePos.toInt
        val clone = new Clone(direction, cloneFloor, clonePos)
        clones += clone

        // Write an action using println
        // To debug: Console.err.println("Debug messages...")
        val elevator = elevators.find(_.floor == cloneFloor)
        // if(clones.head.leading && cloneFloor == 0) clone.setup else 
        if(direction != "NONE") clone.play(elevator, exit) else println("WAIT")
        // println("WAIT") // action: WAIT or BLOCK
    }
}
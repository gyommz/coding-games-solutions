import math._
import scala.util._
import scala.io.StdIn._
import scala.util.control._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {

    class Stone(var lvl: Int, var amount: Int) {
        this.lvl = lvl
        this.amount = amount

        def addStoneAmount() {
            this.amount = this.amount + 1
        }

        def combine() {
            // if 2 stones can combine
            if (this.amount >= 2) {
                var stonePlusOneIndex = checkIfStoneOfSameLvlExist(stones, this.lvl + 1)
                var amountLeft = this.amount %2
                var amountToDivide = this.amount - amountLeft
                
                // add amount to the stone of +1 lvl
                if (stonePlusOneIndex != -1) {
                    Console.err.println("amount before fusion " + stones( stonePlusOneIndex).amount)
                    stones(stonePlusOneIndex).amount += amountToDivide / 2
                    Console.err.println("amount after fusion " + stones( stonePlusOneIndex).amount)
                } 
                // add a new stone in stones array
                else {
                    stones = stones :+ new Stone(this.lvl + 1, (this.amount - amountLeft) / 2 )
                }
                this.amount = amountLeft
            }
        }
    }

    // number of stone
    val n = readLine.toInt

    // array of stone
    var stones: Array[Stone] = Array()

    var inputs = readLine split " "

    for(i <- 0 until n) {

        val level = inputs(i).toInt
        var find = false
        var numberOfStones = stones.length
        
        if (numberOfStones != 0) {
            for(j <- 0 until numberOfStones) {
                if(stones(j).lvl == level) {
                    stones(j).addStoneAmount
                    find = true
                }
            }
        }
    
        if (!find) {
            stones = stones :+ new Stone(level, 1)
        }
    }

    def checkIfStoneOfSameLvlExist(array: Array[Stone], lvl: Int): Int = {

        for (i <- 0 until array.length) {
            if (array(i).lvl == lvl) {
                return i
            }
        }
        return -1
    }

    def countStones(array: Array[Stone]) {

        var numberOfStones = 0

        for (stone <- array) {
            numberOfStones += stone.amount
        }
        println(numberOfStones)
    }

    def canCombine(arr: Array[Stone]): Boolean = {

        for (stone <- arr) {
            if(stone.amount >= 2) {
                return true
            }
        }
        return false
    }

    def combineAllStones(array: Array[Stone]) {

        for(i <- 0 until array.length) {
            if (array(i).amount >= 2) {
                array(i).combine()
            }
        }
    }
    
    var loop = new Breaks

    loop.breakable {
        
        while(true) {

            if(canCombine(stones) == false) {
                loop.break
            }

            combineAllStones(stones)
        }
    }

    countStones(stones)
   

    // Write an answer using println
    // To debug: Console.err.println("Debug messages...")
    
    
}
import math._
import scala.util._
import scala.util.control._
import scala.io.StdIn._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {

    class Cage(var sick: Int, var healthy: Int, var alive: Int) {
        this.sick = sick;
        this.healthy = healthy;
        this.alive = alive;
    }

    def contamination(cage: Cage) {
        for (i <- 0 until cage.sick) {
            if (cage.healthy >= 2) {
                cage.healthy -= 2
                cage.sick += 1
            } else if (cage.healthy == 1) {
                cage.healthy -= 1
            } else {
                cage.sick -= 1
            }
        }
        cage.alive = cage.sick + cage.healthy
    }

    def countPorcupine(cages: Array[Cage]) {
        for (i <- 0 until cages.length) {
            numberOfPorcupine += cages(i).alive
        }
    }

    // number of porcupine
    var numberOfPorcupine = 0

    // number of cages
    val n = readLine.toInt

    // array of cages
    var cages = new Array[Cage](n)

    // number of years
    val y = readLine.toInt

    // loop all cages
    for(i <- 0 until n) {
        val Array(s, h, a) = (readLine split " ").filter(_ != "").map (_.toInt)
        cages(i) = new Cage(s, h, a)
    }

    var loop = new Breaks;

    // loop all years
    loop.breakable {
        for(i <- 0 until y) {
            for(cage <- cages) {
                if (cage.sick > 0 && cage.alive > 0) {
                    contamination(cage) 
                }
            }

            numberOfPorcupine = 0
            countPorcupine(cages)
            // Write an answer using println
            // To debug: Console.err.println("Debug messages...")
            
            println(numberOfPorcupine) 
            

            if(numberOfPorcupine == 0) { 
                loop.break
            }   
        }
    }
}
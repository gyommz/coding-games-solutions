/**
 * Save the Planet.
 * Use less Fossil Fuel.
 **/

interface Position {
    x: number;
    y: number;
}

class Point {
    pos: Position; 

    constructor(pos: Position) {
        this.pos = pos;
    }
}

class Ship {
    pos: Position;
    rotation: number;
    verticalSpeed: number;
    horizontalSpeed: number;

    constructor(pos: Position, rotation: number, verticalSpeed: number, horizontalSpeed: number) {
        this.pos = pos
        this.rotation = rotation
        this.verticalSpeed = verticalSpeed
        this.horizontalSpeed = horizontalSpeed
    }

    shouldKeepAltitude(site: Site) {
        return ((this.pos.y - site.heigth) < 600 && getDistanceToSite(this.pos.x, site) > 1200) ? true : false;
    }

    getPower(site: Site) {
        let HScomp = 0;
        let VScomp = 0;
        let pow = 0;
        if(this.shouldKeepAltitude(site) && this.verticalSpeed < -1) {
            pow = 4;
        } else {
            if((this.rotation < 0 && this.horizontalSpeed < 0) || (this.rotation > 0 && this.horizontalSpeed > 0)) {
                HScomp = Math.abs(Math.round(this.horizontalSpeed / 15.0));  
            } else {
                HScomp = 0;
            }
            VScomp = -Math.round(this.verticalSpeed / 6.6);
            pow = Math.min(HScomp + VScomp, 4);
        }
        if(getDistanceToSite(this.pos.x, site) == 0 && this.rotation == 0 && (this.pos.y - site.heigth) < 123 && this.verticalSpeed > -30) {
            console.error("Power Off !");
            pow = 0;
        }
    
        console.error("HScomp:" + HScomp + ", VScomp:" + VScomp + ", pow:" + pow);
        return pow;
    }

    getRotation(site: Site) {
        let angleXcomp = 0;                      
        let angleHScomp = 0;                        
        let angle = 0;   
    
        if(this.shouldKeepAltitude(site) && this.horizontalSpeed != 0) {
            angle = 0;
        } else {
            angleXcomp = Math.round(getDistanceToSite(this.pos.x, site) * (3 / 185.0));
            angleXcomp += (angleXcomp / 0.67);
            if (Math.abs(this.horizontalSpeed) > 7 && this.pos.y > site.heigth + 100) {
                angleHScomp = Math.round(this.horizontalSpeed * (9 / 24.75));
                angleHScomp += (angleHScomp / 0.7);
            } else {
                angleHScomp = 0;
            }
            angle = angleXcomp + angleHScomp;
            if (angle > 90 || angle < -90) {
                angle = angle > 90 ? 90 : -90;
            }
        }
        return Math.round(angle);
    }

    land(site: Site) {
        console.log(`${this.getRotation(site)} ${this.getPower(site)}`)
    }
    
}

class Site {
    leftX: number;
    rigthX: number;
    heigth: number;

    constructor(leftX: number, rigthX: number, heigth: number) {
        this.leftX = leftX;
        this.rigthX = rigthX;
        this.heigth = heigth;
    }
}

function getDistanceToSite(x: number, site: Site) {
    if(x < site.leftX) {
        return x - site.leftX;
    } else if (x >= site.leftX && x <= site.rigthX) {
        return 0;
    } else {
        return x - site.rigthX;
    }
}

function getSite(surface: Point[]) {
    for(let i = 0; i < surface.length - 1; i++) {
        if(surface[i].pos.y === surface[i + 1].pos.y) {
            return new Site(surface[i].pos.x, surface[i + 1].pos.x, surface[i].pos.y);
        }
    }
}

let surfaces: Point[] = [];

const N: number = parseInt(readline()); // the number of points used to draw the surface of Mars.
for (let i = 0; i < N; i++) {
    var inputs: string[] = readline().split(' ');
    const landX: number = parseInt(inputs[0]); // X coordinate of a surface point. (0 to 6999)
    const landY: number = parseInt(inputs[1]); // Y coordinate of a surface point. By linking all the points together in a sequential fashion, you form the surface of Mars.
    surfaces.push(new Point({x: landX, y: landY}));
}

const site = getSite(surfaces);

// game loop
while (true) {
    var inputs: string[] = readline().split(' ');
    const X: number = parseInt(inputs[0]);
    const Y: number = parseInt(inputs[1]);
    const HS: number = parseInt(inputs[2]); // the horizontal speed (in m/s), can be negative.
    const VS: number = parseInt(inputs[3]); // the vertical speed (in m/s), can be negative.
    const F: number = parseInt(inputs[4]); // the quantity of remaining fuel in liters.
    const R: number = parseInt(inputs[5]); // the rotation angle in degrees (-90 to 90).
    const P: number = parseInt(inputs[6]); // the thrust power (0 to 4).

    const lander = new Ship({x: X, y: Y}, R, VS, HS);
    // Write an action using console.log()
    // To debug: console.error('Debug messages...');


    // R P. R is the desired rotation angle. P is the desired thrust power.
    lander.land(site);
}

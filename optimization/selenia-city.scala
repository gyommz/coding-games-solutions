import math._
import scala.util._
import scala.io.StdIn._

trait Building {
  def id: Int
  def coordX: Int
  def coordY: Int
}

case class TravelRoute(buildingId1: Int, buildingId2: Int, capacity: Int)

case class Pod(id: Int, path: List[Int])

case class AstronautGroup(typee: Int, count: Int)

case class LandingPad(
    id: Int, 
    coordX: Int, 
    coordY: Int, 
    astronautsPerMonth: Int, 
    astronautGroups: List[AstronautGroup]
) extends Building

case class LunarModule(moduleType: Int, id: Int, coordX: Int, coordY: Int) extends Building

case class GameState(
  resources: Int,
  travelRoutes: List[TravelRoute],
  pods: List[Pod],
  buildings: List[Building]
)

case class AccumulatedState(
    gameState: GameState,
    allBuildings: List[Building]
)

object ActionType extends Enumeration {
  type ActionType = Value
  val TUBE, UPGRADE, TELEPORT, POD, DESTROY, WAIT = Value
}

case class Action(actionType: ActionType.Value, parameters: List[String])

case class Point(x: Double, y: Double)

object Geometry {
  def distance(p1: Point, p2: Point): Double = 
    sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2))

  def pointOnSegment(a: Point, b: Point, c: Point): Boolean = {
    val epsilon = 0.0000001
    val distanceSum = distance(b, a) + distance(a, c)
    val segmentLength = distance(b, c)
    math.abs(distanceSum - segmentLength) < epsilon
  }
}

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Player extends App {

    // PARSE

    def parseTravelRoute(input: String): TravelRoute = {
        val Array(buildingId1, buildingId2, capacity) = input.split(" ").map(_.toInt)
        TravelRoute(buildingId1, buildingId2, capacity)
    }

    def parsePod(input: String): Pod = {
        val parts = input.split(" ").map(_.toInt)
        Pod(parts(0), parts.drop(2).toList)
    }

   def parseBuilding(input: String): Building = {
        val parts = input.split(" ").map(_.toInt)
        parts(0) match {
            case 0 => 
                val id = parts(1)
                val coordX = parts(2)
                val coordY = parts(3)
                val astronautsPerMonth = parts(4)
                val astronautTypes = parts.drop(5)
                val groups = astronautTypes
                    .groupBy(identity)
                    .map { case (typee, list) => AstronautGroup(typee, list.size) }
                    .toList
                    .sortBy(_.typee)
                LandingPad(id, coordX, coordY, astronautsPerMonth, groups)
            case moduleType => LunarModule(moduleType, parts(1), parts(2), parts(3))
        }
    }

    // READ

    def readResources: Int = readLine.toInt
    def readTravelRoutes: List[TravelRoute] = {
        val numTravelRoutes = readLine.toInt
        (1 to numTravelRoutes).map(_ => parseTravelRoute(readLine)).toList
    }
    def readPods: List[Pod] = {
        val numPods = readLine.toInt
        (1 to numPods).map(_ => parsePod(readLine)).toList
    }
    def readNewBuildings: List[Building] = {
        val numNewBuildings = readLine.toInt
        (1 to numNewBuildings).map(_ => parseBuilding(readLine)).toList
    }

    // main logic here
    // TUBE | UPGRADE | TELEPORT | POD | DESTROY | WAIT
    val generateActions = (gameState: GameState) => List(
        Action(ActionType.TUBE, List("0", "1")),
        Action(ActionType.TUBE, List("0", "2")),
        Action(ActionType.POD, List("42", "0", "1", "0", "2", "0", "1", "0", "2"))
    )

    val actionsToString = (actions: List[Action]) =>
        actions.map(action => s"${action.actionType} ${action.parameters.mkString(" ")}").mkString(";")

    val toAction = (action: String) => println(action)

    def logGameState(accumulatedState: AccumulatedState): Unit = {
        val gameState = accumulatedState.gameState
        Console.err.println("=== Game State ===")
        Console.err.println(s"Resources: ${gameState.resources}")
        
        Console.err.println("\nTravel Routes:")
        gameState.travelRoutes.foreach { route =>
            Console.err.println(s"  From ${route.buildingId1} to ${route.buildingId2} (Capacity: ${route.capacity})")
        }
        
        Console.err.println("\nPods:")
        gameState.pods.foreach { pod =>
            Console.err.println(s"  Pod ${pod.id}: Path = ${pod.path.mkString(" -> ")}")
        }
        
        Console.err.println("\nCurrent Turn Buildings:")
        gameState.buildings.foreach(logBuilding)
        
        Console.err.println("\nAll Known Buildings:")
        accumulatedState.allBuildings.foreach(logBuilding)
        
        Console.err.println("==================")
    }

    private def logBuilding(building: Building): Unit = building match {
        case LandingPad(id, x, y, astronautsPerMonth, astronautGroups) =>
            val typesString = astronautGroups.map(g => s"${g.count} type ${g.typee}").mkString(", ")
            Console.err.println(s"  Landing Pad $id at ($x, $y): $astronautsPerMonth astronauts per month (${typesString})")
        case LunarModule(moduleType, id, x, y) =>
            Console.err.println(s"  Lunar Module $id (Type $moduleType) at ($x, $y)")
    }

    def setupInitialState: AccumulatedState = {
        val setup: Unit => (Int, List[TravelRoute], List[Pod], List[Building]) = _ => {
            val resources = readResources
            val travelRoutes = readTravelRoutes
            val pods = readPods
            val newBuildings = readNewBuildings
            (resources, travelRoutes, pods, newBuildings)
        }
        
        val createGameState: ((Int, List[TravelRoute], List[Pod], List[Building])) => GameState = 
            { case (r, tr, p, b) => GameState(r, tr, p, b) }
        
        val initialGameState = (createGameState compose setup)(())
        AccumulatedState(initialGameState, initialGameState.buildings)
    }

    def updateState(prevState: AccumulatedState): AccumulatedState = {
        val newResources = readResources
        val newTravelRoutes = readTravelRoutes
        val newPods = readPods
        val newBuildings = readNewBuildings

        val updatedBuildings = (prevState.allBuildings ++ newBuildings).distinct
        val updatedGameState = GameState(newResources, newTravelRoutes, newPods, updatedBuildings)
        
        AccumulatedState(updatedGameState, updatedBuildings)
    }

    val gameLoop: AccumulatedState => AccumulatedState = state => {
        logGameState(state)
        val actions = generateActions(state.gameState)
        val actionString = actionsToString(actions)
        toAction(actionString)
        updateState(state)
    }

    // Initial setup
    var accumulatedState = setupInitialState

    while(true) {
        accumulatedState = gameLoop(accumulatedState)
        gameLoop(accumulatedState)
    }
}
import math._
import scala.util._
import scala.io.StdIn._
import scala.collection.mutable.ArrayBuffer
import scala.util.control.Breaks._

// basePoint  = HumanCount * 10
// bonusPoint = combo * nb human alive
// totalPoint = basePoint * totalPoint

case class Position(x: Int, y: Int)

trait Creature {
    val id: Int
    val position: Position
}

class Zombie(val id: Int, val position: Position, val nextPosition: Position) extends Creature {
    val speed = 400

    def isCloser(zombiesDist: Double, playerDist: Double): Boolean = {
        zombiesDist < playerDist
    }

    def nextTargetIsPlayer(creatures: Array[Human], player: Player): Boolean = {
        creatures.forall(human => isCloser(
                Utils.getDistance(human.position, this.position), 
                Utils.getDistance(human.position, player.position)
            )
        )
    }

    def closestHuman(human1: Human, human2: Human): Human = {
        if(Utils.getDistance(this.position, human1.position) < Utils.getDistance(this.position, human2.position)) human1 else human2
    }
    
    def getClosestHuman(humans: Array[Human]): Human = {
        humans.reduceLeft(closestHuman)
    }
}

class Human(val id: Int, val position: Position) extends Creature {

    def closestZombie(zombie1: Zombie, zombie2: Zombie): Zombie = {
        if(Utils.getDistance(this.position, zombie1.position) < Utils.getDistance(this.position, zombie2.position)) zombie1 else zombie2
    }
    
    def getClosestZombie(zombies: Array[Zombie]): Zombie = {
        zombies.reduceLeft(closestZombie)
    }
}

object Utils {

    def getDistance(pos1: Position, pos2: Position): Double = {
        Math.sqrt(Math.pow(pos2.x - pos1.x, 2) + Math.pow(pos2.y - pos1.y, 2))
    }

    def turnToDestination(distance: Double, speed: Int): Double = {
        distance.toDouble / speed
    }

    def inKillRange(distance: Double, killRange: Int): Boolean = {
        distance < killRange.toDouble
    }

    def getTopOrBottom(position: Position): Position = { 
        if(getDistance(position, Position(0, position.y)) < getDistance(position, Position(Player.XMAX, position.y))) Position(0, position.y) else Position(Player.XMAX, position.y)
    }

    def zombiesIsCloseToZombie(zombie: Zombie, otherZombie: Zombie): Boolean = {
        (otherZombie.position.x <= zombie.position.x + 200 && zombie.position.y <= zombie.position.y + 200) && (otherZombie.position.x >= zombie.position.x + 200 && otherZombie.position.y >= zombie.position.y + 200)
    }

    def isZombiesPack(zombies: Array[Zombie]): Boolean = {
        zombies.forall(zombiesIsCloseToZombie(zombies.head, _))
    }

    def reachableHumans(humans: Array[Human], zombies: Array[Zombie], player: Player): Array[Human] = {
        humans.filter((human: Human) => player.canReachHuman(human, human.getClosestZombie(zombies)))
    }

    def getFirstHumanToSave(humans: Array[Human], zombies: Array[Zombie], player: Player): Human = {
        // humans.reduceLeft((human: Human) => getDistance(human.getClosestZombie(zombies).position, human.position)  )
        var firstToDie = humans.head
        var firstDistance = getDistance(firstToDie.position, firstToDie.getClosestZombie(zombies).position)
        if(humans.length > 1) {
            for(human <- humans) {
                val closestZombie = human.getClosestZombie(zombies)
                val distance = getDistance(closestZombie.position, human.position)
                if(distance < firstDistance) {
                    firstDistance = distance
                    firstToDie = human
                }
            }
        }
        return firstToDie
    }
}

class Player(val position: Position) {
    val speed: Int = 1000
    val killRange: Int = 2000

    def manage(humans: Array[Human], zombies: Array[Zombie]) {
        if(Utils.isZombiesPack(zombies)) move(Utils.getFirstHumanToSave(Utils.reachableHumans(humans, zombies, this), zombies, this).getClosestZombie(zombies).position)

        if(Utils.getFirstHumanToSave(Utils.reachableHumans(humans, zombies, this), zombies, this)
            .getClosestZombie(zombies)
            .nextTargetIsPlayer(humans, this)) 
            
        move(Utils.getTopOrBottom(
                Utils.getFirstHumanToSave(Utils.reachableHumans(humans, zombies, this), zombies, this)
                .getClosestZombie(zombies)
                .nextPosition)) 
        else 

        move(Utils.getFirstHumanToSave(Utils.reachableHumans(humans, zombies, this), zombies, this)
                .getClosestZombie(zombies)
                .position)
    }

    def canReachHuman(human: Human, zombie: Zombie): Boolean = {
        Utils.turnToDestination(Utils.getDistance(this.position, human.position) - this.killRange, this.speed + 1) <= Utils.turnToDestination(Utils.getDistance(human.position, zombie.position), zombie.speed)
    }
    
    def move(position: Position): Unit = {
        println(position.x + " " + position.y)
    }
}

/**
 * Save humans, destroy zombies!
 **/
object Player extends App {
    val XMAX = 15999
    val YMAX = 8999

    // game loop
    while(true) {
        val Array(x, y) = (readLine split " ").filter(_ != "").map (_.toInt)
        val ash = new Player(Position(x, y))

        val humanCount = readLine.toInt
        val humans = new Array[Human](humanCount)
        for(i <- 0 until humanCount) {
            val Array(humanId, humanX, humanY) = (readLine split " ").filter(_ != "").map (_.toInt)
            humans(i) = new Human(humanId, Position(humanX, humanY))
        }
        
        val zombieCount = readLine.toInt
        val zombies = new Array[Zombie](zombieCount)
        for(i <- 0 until zombieCount) {
            val Array(zombieId, zombieX, zombieY, zombieXNext, zombieYNext) = (readLine split " ").filter(_ != "").map (_.toInt)
            zombies(i) = new Zombie(zombieId, Position(zombieX, zombieY), Position(zombieXNext, zombieYNext)) 
        }
        Console.err.println(Utils.getDistance(zombies(0).getClosestHuman(humans).position, zombies(0).position))
        ash.manage(humans, zombies)
        // Write an action using println
        // To debug: Console.err.println("Debug messages...")
        // println("0 0") // Your destination coordinates
    }
}
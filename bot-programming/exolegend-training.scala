import math._
import scala.util._
import scala.io.StdIn._
import scala.collection.mutable.PriorityQueue
import scala.util.Random


object Helper {
  object PacClass extends Enumeration {
    type PacClass = Value
    val ROCK, PAPER, SCISSORS, DEAD = Value
  }

  object CellType extends Enumeration {
    type CellType = Value
    val Wall, Floor = Value
  }

  case class Coordinate(x: Int, y: Int)

  import PacClass._
  import CellType._

  case class Pac(id: Int, mine: Boolean, pos: Coordinate, pacType: PacClass.Value) {
    def command(pos: Coordinate): String = {
      s"MOVE $id ${pos.x} ${pos.y}"
    }

    def canKill(pac: Pac): Boolean = (pacType, pac.pacType) match {
      case (ROCK, SCISSORS) => true
      case (PAPER, ROCK) => true
      case (SCISSORS, PAPER) => true
      case _ => false
    }
  }

  case class Pellet(pos: Coordinate, value: Int)

  case class Cell(pos: Coordinate, cellType: CellType.Value)

  def rowToCells(row: String, y: Int): List[Cell] = {
    row.zipWithIndex.map { case (char, x) =>
      val coordinate = Coordinate(x, y)
      val cellType = if (char == ' ') Floor else Wall
      Cell(coordinate, cellType)
    }.toList
  }

  def debug(x: Any): Unit = {
    Console.err.println(s"Debug: $x")
  }
}

import Helper._

object GameLogic {
    
    case class ActionPriority(target: Coordinate, priority: Int, pacId: Int)

    def updateGameState(pacs: Map[Boolean, List[Pac]], pellets: List[Pellet], gridCells: List[Cell]): Unit = {
        debug(s"Number of pacs: ${pacs.values.flatten.size}")
        debug(s"Number of pellets: ${pellets.size}")
        debug(s"Number of grid cells: ${gridCells.size}")

        val allActions = pacs.values.flatten.map(myPac => prioritizeActions(myPac, pacs(!myPac.mine), pellets, gridCells))
        val prioritizedActions = prioritizePacActions(allActions.flatten.toList)
        debug(s"prio actions count ${prioritizedActions.length}")
        val pacActionsByPacId = prioritizedActions.groupBy(_.pacId)
        val uniqueActions = pacActionsByPacId.values.collect {
            case actions if actions.nonEmpty => actions.maxBy(_.priority)
        }.toList
        val pacActionsString = uniqueActions.map(action => s"MOVE ${action.pacId} ${action.target.x} ${action.target.y}").mkString(" | ")


        val tmp = allActions.flatten.toList.distinctBy(_.pacId).map(action => s"MOVE ${action.pacId} ${action.target.x} ${action.target.y}").mkString(" | ")
        println(tmp)
    }


    def prioritizePacActions(allActions: List[ActionPriority]): List[ActionPriority] = {
        val groupedActions = allActions.groupBy(_.priority)

        val sortedGroups = groupedActions.toSeq.sortBy(_._1)

        val result = sortedGroups.flatMap { case (_, actions) =>
            scala.util.Random.shuffle(actions)
        }

        result.toList
    }



    def prioritizeActions(myPac: Pac, enemyPacs: List[Pac], pellets: List[Pellet], gridCells: List[Cell]): List[ActionPriority] = {
        val enemyPacsToKill = findEnemyPacsToKill(myPac, enemyPacs)
        val superPellets = pellets.filter(_.value == 10)
        val normalPellets = pellets.filter(_.value != 10)

        val killActions = enemyPacsToKill.map { enemyPac =>
            ActionPriority(enemyPac.pos, 1, myPac.id)
        }

        val superPelletActions = superPellets.map { superPellet =>
            ActionPriority(superPellet.pos, 2, myPac.id)
        }

        val normalPelletActions = normalPellets.map { normalPellet =>
            ActionPriority(normalPellet.pos, 3, myPac.id)
        }

        val exploreActions = List(ActionPriority(randomFloorPosition(gridCells).get, 4, myPac.id))

        val allActions = killActions ++ superPelletActions ++ normalPelletActions ++ exploreActions
        debug(allActions.length)
        exploreActions
    }

    private def calculateDistance(coord1: Coordinate, coord2: Coordinate): Int = {
        Math.abs(coord2.x - coord1.x) + Math.abs(coord2.y - coord1.y)
    }

    private def findEnemyPacsToKill(myPac: Pac, enemyPacs: List[Pac]): List[Pac] = {
        enemyPacs.filter(enemyPac => myPac.canKill(enemyPac))
    }

    private def findEnemyPacsThatCanKillMe(myPac: Pac, enemyPacs: List[Pac]): List[Pac] = {
        enemyPacs.filter(enemyPac => enemyPac.canKill(myPac))
    }

    private def randomFloorPosition(gridCells: List[Cell]): Option[Coordinate] = {
        val floorCells = gridCells.filter(_.cellType == CellType.Floor)
        if (floorCells.nonEmpty) {
            val randomIndex = Random.nextInt(floorCells.length)
            Some(floorCells(randomIndex).pos)
        } else {
            None
        }
    }
}

import GameLogic._

/**
 * Grab the pellets as fast as you can!
 **/
object Player extends App {
  // width: size of the grid
  // height: top left corner is (x=0, y=0)
  val Array(width, height) = (readLine split " ").filter(_ != "").map(_.toInt)
  val gridCells: List[Cell] = (0 until height).flatMap { y =>
    val row = readLine
    rowToCells(row, y)
  }.toList

  // game loop
  while (true) {
    val Array(myScore, opponentScore) = (readLine split " ").filter(_ != "").map(_.toInt)

    // all your pacs and enemy pacs in sight
    val pacList: List[Pac] = (0 until readLine.toInt).map {
      _ => val Array(_pacId, _mine, _x, _y, typeId) = readLine split " "
        Pac(_pacId.toInt, _mine.toInt != 0, Coordinate(_x.toInt, _y.toInt), PacClass.withName(typeId))
    }
    .toList
    .filter(p => p.pacType != "DEAD")

    val pacs: Map[Boolean, List[Pac]] = pacList.groupBy(
      pac => pac.mine
    )

    // all pellets in sight
    val pelletList: List[Pellet] = (0 until readLine.toInt).map {
      _ => val Array(x, y, value) = (readLine split " ").filter(_ != "").map(_.toInt)
        Pellet(Coordinate(x, y), value)
    }.toList

    debug("start")
    updateGameState(pacs, pelletList, gridCells)
  }
}

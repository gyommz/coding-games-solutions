import math._
import scala.util._
import scala.io.StdIn._

trait Data {
    val id: Int
    val color: Int
    val _type: Int
}

case class FishData(id: Int, color: Int, _type: Int) extends Data
case class Position(x: Int, y: Int)
case class Fish(id: Int, color: Int, _type: Int, pos: Position, speed: Position) extends Data


class Drone(id: Int, pos: Position, emergency: Int, batteryLevel: Int) {

  def handle(fishlist: List[Fish]): Unit = {
    findClosestFishPos(fishlist)
      .map(closestFishPos => move(closestFishPos, 1))
  }

  def findClosestFishPos(fishList: List[Fish]): Option[Position] =
    fishList.filterNot(fish => isInsideRadius(fish.pos, radius = 800))
      .minByOption(p => distance(this.pos, p.pos))
      .map(_.pos)

  def isInsideRadius(targetPos: Position, radius: Double): Boolean =
    distance(this.pos, targetPos) <= radius

  def move(pos: Position, light: Int): Unit =
    println(s"MOVE ${pos.x} ${pos.y} $light")

  def distance(p1: Position, p2: Position): Double =
    Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2))
}



/**
 * Score points by scanning valuable fish faster than your opponent.
 **/
object Player extends App {
    val creatureCount = readLine.toInt
    val fishDatas: List[FishData] = (0 until creatureCount).foldLeft(List.empty[FishData]) { (acc, _) => 
        val Array(creatureId, color, _type) = (readLine split " ").filter(_ != "").map (_.toInt)
        acc :+ FishData(creatureId, color, _type)
    }

    // game loop
    while(true) {
        val myScore = readLine.toInt
        val foeScore = readLine.toInt

        val myScanCount = readLine.toInt
        val scannedFishList: List[Int] = (0 until myScanCount).foldLeft(List.empty[Int]) { (acc, _) =>
            acc :+  readLine.toInt
        }
        
        scannedFishList.foreach((fish) => Console.err.println(s"fish ${fish}"))


        val foeScanCount = readLine.toInt
        for(i <- 0 until foeScanCount) {
            val creatureId = readLine.toInt
        }
        // Me
        val myDroneCount = readLine.toInt
        val myDrone: List[Drone] =(0 until myDroneCount).foldLeft(List.empty[Drone]) { (acc, _) =>
            val Array(droneId, droneX, droneY, emergency, battery) = (readLine split " ").filter(_ != "").map (_.toInt)
            acc :+ new Drone(droneId, Position(droneX, droneY), emergency, battery)
        }

        // Enemy
        val foeDroneCount = readLine.toInt
        val enemyDrone: List[Drone] =(0 until myDroneCount).foldLeft(List.empty[Drone]) { (acc, _) =>
            val Array(droneId, droneX, droneY, emergency, battery) = (readLine split " ").filter(_ != "").map (_.toInt)
            acc :+ new Drone(droneId, Position(droneX, droneY), emergency, battery)
        }

        val droneScanCount = readLine.toInt
        for(i <- 0 until droneScanCount) {
            val Array(droneId, creatureId) = (readLine split " ").filter(_ != "").map (_.toInt)
        }

        val visibleCreatureCount = readLine.toInt
        val visibleFishList: List[Fish] = (0 until visibleCreatureCount).foldLeft(List.empty[Fish]) { (acc, _) =>
            val Array(creatureId, creatureX, creatureY, creatureVx, creatureVy) = (readLine split " ").filter(_ != "").map(_.toInt)
            val info = fishDatas.find(_.id == creatureId).get
            val fish = Fish(info.id, info.color, info._type, Position(creatureX, creatureY), Position(creatureVx, creatureVy))
            acc :+ fish
        }
            
        val radarBlipCount = readLine.toInt
        for(i <- 0 until radarBlipCount) {
            val Array(_droneId, _creatureId, radar) = readLine split " "
            val droneId = _droneId.toInt
            val creatureId = _creatureId.toInt
        }
        for(i <- 0 until myDroneCount) {
            
            // Write an action using println
            // To debug: Console.err.println("Debug messages...")
            myDrone(0).handle(
                visibleFishList.filterNot(fish => scannedFishList.contains(fish.id))
            )
            
        }
    }
}
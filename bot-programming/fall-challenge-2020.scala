import math._
import scala.util._
import scala.io.StdIn._
import scala.collection.mutable._

case class Spell(val id: Int, val gain: Ingredient, val cost: Option[Ingredient], val castable: Boolean)

case class Ingredient(val tier: Int, val amount: Int)

class Witch(val inventory: List[Ingredient], val score: Int) {
    def brew(potionId: Int): Unit = {
        println(s"BREW $potionId")
    }

    def cast(spellId: Int) : Unit = {
        println(s"CAST $spellId")
    }

    def rest(): Unit = {
        println("REST")
    }

    def amounntOfIngr(tier: Int): Int = {
        inventory.filter(_.tier == tier).map(_.amount).sum
    }
    
    def canBrew(potion: Potion): Boolean = {
        potion.requirements.forall(req => {req.amount <= amounntOfIngr(req.tier)})
    }
}

case class Potion(val id: Int, val requirements: List[Ingredient], val price: Int)

class Shop {
    var clientOrders = new ListBuffer[Potion]

    def addOrder(potionId: Int, ingredients: List[Ingredient], price: Int): Unit = {
        clientOrders.append(Potion(potionId, ingredients.filter(_.amount != 0), price))
    }
}

object Choices {
    def findMostValuablePotion(potions: ListBuffer[Potion], witch: Witch): Potion = {
        potions.filter(witch.canBrew(_)).sortWith(_.price > _.price).head
    }

}

object SpellHandler {
    def giveSpell(teamSpells: ListBuffer[Spell], id: Int, ingredients: List[Ingredient], castable: Boolean): Unit = {
        addSpell(teamSpells, id, findSpellGain(ingredients), findSpellCost(ingredients), castable)
    }

    def findSpellCost(ingredients: List[Ingredient]): Option[Ingredient] = {
        if(ingredients.filter(_.amount < 0).length != 0) Some(ingredients.filter(_.amount.signum == -1).head) else None
    }

    def findSpellGain(ingredients: List[Ingredient]): Ingredient = {
        ingredients.filter(_.amount > 0).head
    }

    def addSpell(spellList: ListBuffer[Spell], spellId: Int, spellGain: Ingredient, spellCost: Option[Ingredient], castable: Boolean): Unit = {
        spellList.append(Spell(spellId, spellGain, spellCost, castable))
    }

    def castables(teamSpells: ListBuffer[Spell]): ListBuffer[Spell] = {
        teamSpells.filter(_.castable)
    }
}

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Player extends App {

    // game loop
    while(true) {
        val shop = new Shop
        val mySpells = new ListBuffer[Spell]
        val opponentSpells = new ListBuffer[Spell]
        val actionCount = readLine.toInt // the number of spells and recipes in play
        for(i <- 0 until actionCount) {
            // actionId: the unique ID of this spell or recipe
            // actionType: in the first league: BREW; later: CAST, OPPONENT_CAST, LEARN, BREW
            // delta0: tier-0 ingredient change
            // delta1: tier-1 ingredient change
            // delta2: tier-2 ingredient change
            // delta3: tier-3 ingredient change
            // price: the price in rupees if this is a potion
            // tomeIndex: in the first two leagues: always 0; later: the index in the tome if this is a tome spell, equal to the read-ahead tax; For brews, this is the value of the current urgency bonus
            // taxCount: in the first two leagues: always 0; later: the amount of taxed tier-0 ingredients you gain from learning this spell; For brews, this is how many times you can still gain an urgency bonus
            // castable: in the first league: always 0; later: 1 if this is a castable player spell
            // repeatable: for the first two leagues: always 0; later: 1 if this is a repeatable player spell
            val Array(_actionId, actionType, _delta0, _delta1, _delta2, _delta3, _price, _tomeIndex, _taxCount, _castable, _repeatable) = readLine split " "
            val actionId = _actionId.toInt
            val delta0 = _delta0.toInt
            val delta1 = _delta1.toInt
            val delta2 = _delta2.toInt
            val delta3 = _delta3.toInt
            val price = _price.toInt
            val tomeIndex = _tomeIndex.toInt // not until Bronze
            val taxCount = _taxCount.toInt // not until Bronze
            val castable = _castable.toInt != 0 
            val repeatable = _repeatable.toInt != 0 // not until Bronze

            actionType match {
                case "BREW"             => shop.addOrder(actionId, List(Ingredient(0, Math.abs(delta0)), Ingredient(1, Math.abs(delta1)), Ingredient(2, Math.abs(delta2)), Ingredient(3, Math.abs(delta3))), price)
                case "CAST"             => SpellHandler.giveSpell(mySpells, actionId, List(Ingredient(0, delta0), Ingredient(1, delta1), Ingredient(2, delta2), Ingredient(3, delta3)), castable)
                case "OPPONENT_CAST"    => SpellHandler.giveSpell(opponentSpells, actionId, List(Ingredient(0, delta0), Ingredient(1, delta1), Ingredient(2, delta2), Ingredient(3, delta3)), castable)
                case "LEARN"            => Console.err.println(actionType)
                case _ =>
            }
        }

        // shop.clientOrders.foreach(_.requirements.foreach(Console.err.println))
        val witchs = new Array[Witch](2)
        for(i <- 0 until 2) {
            // inv0: tier-0 ingredients in inventory
            // score: amount of rupees
            val Array(inv0, inv1, inv2, inv3, score) = (readLine split " ").filter(_ != "").map (_.toInt)
            witchs(i) = new Witch(List(Ingredient(0, inv0), Ingredient(1, inv1), Ingredient(2, inv2), Ingredient(3, inv3)), score)
        }
        val myWitch = witchs(0)
        val ennemyWitch = witchs(1)
        mySpells.foreach(Console.err.println)
        // Write an action using println
        // To debug: Console.err.println("Debug messages...")
        

        // in the first league: BREW <id> | WAIT; later: BREW <id> | CAST <id> [<times>] | LEARN <id> | REST | WAIT
        println("WAIT")
    }
}